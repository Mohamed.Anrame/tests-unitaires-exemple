#include "soustraction_test.h"
#include "arithmetique.h"
#ifdef HAVE_CONFIG_H
  #include "config.h"
#endif

#include <limits>

// Enregistrer la classe de test dans le registre de la suite
CPPUNIT_TEST_SUITE_REGISTRATION(soustractionTest);

void soustractionTest::setUp() {}

void soustractionTest::tearDown() {}

/* TODO: Ajouter les définitions des méthodes de test de la classe
 * soustractionTest
 */
void soustractionTest::soustraction_normal()
{
  operandeA = 2;
  operandeB = 1;
  CPPUNIT_ASSERT_EQUAL(static_cast<long int>(1),
                       static_cast<long int>(arithmetique::soustraction(operandeA, operandeB)));
                      
operandeB = -3;
CPPUNIT_ASSERT_EQUAL(static_cast<long int>(5),
                     static_cast<long int>(arithmetique::soustraction(operandeA, operandeB)));
}
void soustractionTest::soustraction_zero()
{
operandeA = operandeB = 0;

CPPUNIT_ASSERT_EQUAL(static_cast<long int>(0),
		static_cast<long int>(arithmetique::soustraction(operandeA, operandeB)));
}

void soustractionTest::soustraction_min()
{
  operandeA = std::numeric_limits<int>::lowest();
  operandeB = -1
    CPPUNIT_ASSERT_LESS(static_cast<long int>(operandeA),
                        static_cast<long int>(soustraction::soustraction(operandeA, operandeB)));
}

void soustractionTest::soustraction_max()
{
 operandeA = std::numeric_limits<int>::max();
operandeB = 1;

CPPUNIT_ASSERT_GREATER(static_cast<long int>(operandeA),
			static_cast<long int>(arithmetique::soustraction(operandeA, operandeB)));

}
